const grid = document.querySelector('.grid');
const header = document.querySelector('.header');
const currency1 = document.querySelector(".currency1");
const input = document.querySelector(".amount");
const output = document.querySelector(".output");
const toggle = document.querySelector("input[type='button']");
let chosenCurrency1;
let chosenCurrency2;
let currencies = [];
let fromCurrency = document.getElementById('fromCurrency');
let toCurrency = document.getElementById('toCurrency');
let data;

fetch('https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?json')
  .then(response => response.json())
  .then(json => data = json)

setTimeout(function () {
  header.innerHTML = data[0].exchangedate;

  data.forEach(element => {
    currencies.push(element.cc + ' ' + element.txt);
    grid.insertAdjacentHTML('beforeend',
      `<div class="grid_item">
      <div class="grid_cc">${element.cc}</div>
      <div class="grid_txt">${element.txt}</div>
      <div class="grid_rate">${element.rate}</div>
    </div>`);
  });
}, 300);

input.addEventListener("input", () => {
  getResult()
});

toggle.addEventListener("click", () => {
  let from = toCurrency.value;
  toCurrency.value = fromCurrency.value;
  fromCurrency.value = from;
  getResult()
})

function getResult() {
  let fromRate;
  let toRate;
  let result;
  if (input != 0 && fromCurrency.value != '' && toCurrency.value != '') {
    console.log('result started');
    data.forEach(element => {
      if (element.cc == fromCurrency.value.substr(0, 3).toUpperCase()) {
        fromRate = element.rate;
      }
      if (element.cc == toCurrency.value.substr(0, 3).toUpperCase()) {
        toRate = element.rate;
      }
    });
    let result = fromRate / toRate * input.value;
    output.innerHTML = result.toFixed(2);
  }
}

function noDigits(event) {
  if ("1234567890".indexOf(event.key) != -1)
    event.preventDefault();
}

//Info by https://www.w3schools.com/howto/tryit.asp?filename=tryhow_js_autocomplete

function autocomplete(inp, arr) {
  var currentFocus;
  inp.addEventListener("input", function (e) {
    var a, b, i, val = this.value;
    closeAllLists();
    if (!val) { return false; }
    currentFocus = -1;
    a = document.createElement("DIV");
    a.setAttribute("id", this.id + "autocomplete-list");
    a.setAttribute("class", "autocomplete-items");
    this.parentNode.appendChild(a);

    for (i = 0; i < arr.length; i++) {
      if (arr[i].substr(0, val.length).toUpperCase() == val.toUpperCase() |
        arr[i].substr(4, val.length).toUpperCase() == val.toUpperCase()) {
        b = document.createElement("DIV");
        b.innerHTML = "<strong>" + arr[i].substr(0, val.length) + "</strong>";
        b.innerHTML += arr[i].substr(val.length);
        b.innerHTML += "<input type='hidden' value='" + arr[i] + "'>";
        b.addEventListener("click", function (e) {
          inp.value = this.getElementsByTagName("input")[0].value;
          getResult()
          closeAllLists();
        });
        a.appendChild(b);
      }
    }
  });

  inp.addEventListener("keydown", function (e) {
    var x = document.getElementById(this.id + "autocomplete-list");
    if (x) x = x.getElementsByTagName("div");
    if (e.keyCode == 40) {
      currentFocus++;
      addActive(x);
    } else if (e.keyCode == 38) {
      currentFocus--;
      addActive(x);
    } else if (e.keyCode == 13) {
      e.preventDefault();
      if (currentFocus > -1) {
        if (x) x[currentFocus].click();
      }
    }
  });

  function addActive(x) {
    if (!x) return false;
    removeActive(x);
    if (currentFocus >= x.length) currentFocus = 0;
    x[currentFocus].classList.add("autocomplete-active");
    getResult()
  }
  function removeActive(x) {
    for (var i = 0; i < x.length; i++) {
      x[i].classList.remove("autocomplete-active");
    }
  }
  function closeAllLists(elmnt) {
    var x = document.getElementsByClassName("autocomplete-items");
    for (var i = 0; i < x.length; i++) {
      if (elmnt != x[i] && elmnt != inp) {
        x[i].parentNode.removeChild(x[i]);
      }
    }
  }

  document.addEventListener("click", function (e) {
    closeAllLists(e.target);
  });
}

autocomplete(document.getElementById("fromCurrency"), currencies);
autocomplete(document.getElementById("toCurrency"), currencies);
