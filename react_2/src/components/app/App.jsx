import React from "react";
import { Title } from "../title/title";
import "./App.css"
import { Cards } from '../cards/cards';
import { dataList } from './../../data/data';



function App() {
    return (
        // <h1>Hello World</h1>
        <>
            <Title />
            <Cards data={dataList}/>
        </>
    )
}

export default App;