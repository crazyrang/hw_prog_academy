import React from "react";
import ReactDOM from "react-dom";
import data from "./data";
import '../src/style.css'

function App() {
    return (
        <div>
            <h1>Курс Валют</h1>
            <div className="currency-grid">
                {data.map((element) => {
                    return (
                        <div className='currency'>
                            <div className='currency-title'> {element.txt} </div>
                            <div className='currency-rate'> {element.rate} </div>
                        </div>
                    )
                })}
            </div>
        </div>
    )
}

ReactDOM.render(<App></App>, document.getElementById("body"));
console.log(data);