const headerWhich = document.querySelector(".header__which"),
    headerTitle = document.querySelector(".header-title"),
    headerCode = document.querySelector(".header__code"),
    headerInfo = document.querySelector(".header__info"),
    key = document.querySelector(".key"),
    code = document.querySelector(".code"),
    which = document.querySelector(".which"),
    cards = document.querySelector(".cards"),
    historyBlock = document.querySelector(".history"),
    welcome = document.querySelector(".welcome");

let events = [];
let arrayKey = [];

window.addEventListener("keyup", (event) => {
    arrayKey.forEach(el => {
        if (event.key.toUpperCase() === el) {
            let length = arrayKey.length;
            historyBlock.removeChild(historyBlock.childNodes[arrayKey.indexOf(el)]);
            arrayKey.splice(arrayKey.indexOf(el), 1);
        }
    })
    key.innerHTML = event.key;
    code.innerHTML = event.code;
    which.innerHTML = event.which;
    headerCode.innerHTML = event.which;
    headerWhich.innerHTML = event.which;
    addLetterHistory(event);
    headerWhich.classList.add('show');
    headerTitle.classList.add('show');
    headerInfo.classList.add('show');
    cards.classList.add('showCards');
    welcome.classList.add('hide');
});


function addLetterHistory(event) {
    let current = event.key.toUpperCase();
    arrayKey.unshift(current);

    let li = document.createElement('li');
    li.innerHTML = arrayKey[0];
    historyBlock.insertAdjacentElement("afterbegin", li);

    if (arrayKey.length > 4) {
        arrayKey = arrayKey.slice(0, 4);
        historyBlock.lastElementChild.remove();
    }

    let pressedKeyInfo = event;
    li.addEventListener("click", getKeyFromHistory);

    function getKeyFromHistory() {
        key.innerHTML = pressedKeyInfo.key;
        code.innerHTML = pressedKeyInfo.code;
        which.innerHTML = pressedKeyInfo.which;
        headerCode.innerHTML = pressedKeyInfo.which;
        headerWhich.innerHTML = pressedKeyInfo.which;
    }
};